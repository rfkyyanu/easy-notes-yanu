package com.example.easynotes.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.model.Category;
import com.example.easynotes.repository.CategoryRepository;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	// Get all category
	@GetMapping("/show-all")
	public List<Category> getAllCategories() {
		return categoryRepository.findAll();
	}
	
	// Add new category
	@PostMapping("/create")
	public Category createCategory(@Valid @RequestBody Category category) {
		return categoryRepository.save(category);
	}
}
