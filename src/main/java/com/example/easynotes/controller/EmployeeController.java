package com.example.easynotes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Employee;
import com.example.easynotes.repository.EmployeeRepository;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
	@Autowired
	EmployeeRepository employeeRepository;
	
	// Get all Employee
	@GetMapping("/show-all")
	public List<Employee> getAllEmployee() {
		return employeeRepository.findAll();
	}
	
	// Get employee based on jobDescription
	@GetMapping("/show/{jobDescription}")
	public HashMap<String, Object> getEmployeeBasedOnJobDesc(@PathVariable(value = "jobDescription") String jobDesc) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Employee> allEmployee = employeeRepository.findAll();
		ArrayList<Employee> resultArrayList = new ArrayList<Employee>();
		
		for (Employee tempEmp : allEmployee) {
			String tempAft = tempEmp.getJobDescription().replace(' ', '-');
			
			if (tempAft.equalsIgnoreCase(jobDesc)) {
				resultArrayList.add(tempEmp);
			}
		}
		
		result.put("Message", "Read " + jobDesc + " Success");
		result.put("Total", resultArrayList.size());
		result.put("Data", resultArrayList);
		
		return result;
	}
	
	// Get all employee above age average
	@GetMapping("/show/above-age-average")
	public HashMap<String, Object> getEmployeeAboveAverageAge() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Employee> allEmployee = employeeRepository.findAll();
		ArrayList<Employee> resultArrayList = new ArrayList<Employee>();
		double ageAvg = ageAverage(allEmployee);
		
		for (Employee tempEmp : allEmployee) {
			if (tempEmp.getAge() > ageAvg) {
				resultArrayList.add(tempEmp);
			}
		}
		
		result.put("Message", "Read Success");
		result.put("Age Average", ageAvg);
		result.put("Total", resultArrayList.size());
		result.put("Data", resultArrayList);
		
		return result;
	}
	
	// Count age average
	public double ageAverage(List<Employee> employeeList) {
		double result = 0;
		
		for (Employee tempEmp : employeeList) {
			result += tempEmp.getAge();
		}
		
		return result / employeeList.size();
	}
	
	// Get all employee with highest salary
	@GetMapping("/show/max-salary")
	public HashMap<String, Object> getEmployeeWithMaxSalary() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Employee> allEmployee = employeeRepository.findAll();
		ArrayList<Employee> resultArrayList = new ArrayList<Employee>();
		double maxSalary = getMaxSalary(allEmployee);
		
		for (Employee tempEmp : allEmployee) {
			if (tempEmp.getSalary() == maxSalary) {
				resultArrayList.add(tempEmp);
			}
		}
		
		result.put("Message", "Read Success");
		result.put("Max Salary", maxSalary);
		result.put("Total", resultArrayList.size());
		result.put("Data", resultArrayList);
		
		return result;
	}
	
	// Get max salary of employee
	public double getMaxSalary(List<Employee> employeeList) {
		double tempMaxSal = 0;
		
		for (Employee tempEmp : employeeList) {
			if (tempEmp.getSalary() > tempMaxSal) {
				tempMaxSal = tempEmp.getSalary();
			}
			
		}
		
		return tempMaxSal;
	}
	
	// Get all employee that contains given name
	@GetMapping("/show/name")
	public HashMap<String, Object> getFilteredEmployeeName(@RequestParam String name) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Employee> allEmployee = employeeRepository.findAll();
		ArrayList<Employee> resultArrayList = new ArrayList<Employee>();
		
		for (Employee tempEmp : allEmployee) {
			if (tempEmp.getName().toLowerCase().contains(name.toLowerCase())) {
				resultArrayList.add(tempEmp);
			}
		}
		
		result.put("Message", "Read Success");
		result.put("Total", resultArrayList.size());
		result.put("Data", resultArrayList);
		
		return result;
	}
	
	// Create new employee
	@PostMapping("create")
	public Employee createEmployee(@Valid @RequestBody Employee employee) {
		return employeeRepository.save(employee);
	}
	
	// Create multiple employee
	@PostMapping("create/multiple")
	public HashMap<String, Object> createMultipleEmployee(@Valid @RequestBody Employee... employeeList) {
		ArrayList<Employee> resultArrayList = new ArrayList<Employee>();
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		for (Employee tempEmp : employeeList) {
			employeeRepository.save(tempEmp);
			resultArrayList.add(tempEmp);
		}
		
		result.put("Message", "Create Success");
		result.put("Total", resultArrayList.size());
		result.put("Data", resultArrayList);
		
		return result;
	}
	
	// Update employee
	@PutMapping("update-employee/{id}")
	public Employee updateEmployee(@PathVariable(value = "id") Long employeeId, @Valid @RequestBody Employee employeeDetails) {
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
		
		employee.setName(employeeDetails.getName());
		employee.setAddress(employeeDetails.getAddress());
		employee.setAge(employeeDetails.getAge());
		employee.setSalary(employeeDetails.getSalary());
		employee.setJobDescription(employeeDetails.getJobDescription());
		
		Employee updatedEmployee = employeeRepository.save(employee);
		return updatedEmployee;
	}
	
	// Delete employee
	@DeleteMapping("delete-employee/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long employeeId) {
		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
		
		employeeRepository.delete(employee);
		
		return ResponseEntity.ok().build();
	}
	
}
