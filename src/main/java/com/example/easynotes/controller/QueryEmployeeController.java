package com.example.easynotes.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.model.Employee;
import com.example.easynotes.repository.EmployeeRepository;

@RestController
@RequestMapping("/api/query/employee")
public class QueryEmployeeController {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	// Show all data in employee table
	@GetMapping("/show-all")
	public HashMap<String, Object> getAllEmployee() {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		
		hashResult.put("Message", "Read Success");
		hashResult.put("Total", employeeRepository.getAllEmployee().size());
		hashResult.put("Data", employeeRepository.getAllEmployee());
		
		return hashResult;
	}
	
	// Create new employee
	@PostMapping("/create-employee")
	public HashMap<String, Object> createEmployee(@Valid @RequestBody Employee employeeDetails) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		int total = employeeRepository.createEmployee(employeeDetails.getName(), employeeDetails.getAddress(), employeeDetails.getAge(), employeeDetails.getJobDescription(), employeeDetails.getSalary());
		
		hashResult.put("Message", "Create Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
	// Update existed employee
	@PutMapping("/update-employee")
	public HashMap<String, Object> updateEmployee(@RequestParam(name = "id") Long employeeId, @Valid @RequestBody Employee employeeDetails) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		int total = employeeRepository.updateEmployee(employeeDetails.getName(), employeeDetails.getAddress(), employeeDetails.getAge(), employeeDetails.getJobDescription(), employeeDetails.getSalary(), employeeId);

		hashResult.put("Message", "Update Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
	// Delete existed employee
	@DeleteMapping("/delete-employee")
	public HashMap<String, Object> deleteEmployee(@RequestParam(name = "id") Long employeeId) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		
		int total = employeeRepository.deleteEmployee(employeeId);
		
		hashResult.put("Message", "Delete Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
}
