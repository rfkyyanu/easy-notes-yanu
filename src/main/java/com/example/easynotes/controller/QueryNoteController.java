package com.example.easynotes.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.model.Note;
import com.example.easynotes.repository.NoteRepository;

@RestController
@RequestMapping("api/query/note")
public class QueryNoteController {
	
	@Autowired
	NoteRepository noteRepository;
	
	// Show all data in note table
	@GetMapping("/show-all")
	public HashMap<String, Object> getAllNotes() {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		
		hashResult.put("Message", "Read Success");
		hashResult.put("Total", noteRepository.getAllNotes().size());
		hashResult.put("Data", noteRepository.getAllNotes());
		
		return hashResult;
	}
	
	// Create new note
	@PostMapping("/create-notes")
	public HashMap<String, Object> createNote(@Valid @RequestBody Note noteDetails) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		int total = noteRepository.createNotes(noteDetails.getTitle(), noteDetails.getContent());
		
		hashResult.put("Message", "Create Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
	// Update existed note
	@PutMapping("/update-notes")
	public HashMap<String, Object> updateNote(@RequestParam(name = "id") Long noteId, @Valid @RequestBody Note noteDetails) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		List<Note> allEmployee = noteRepository.getAllNotes();
		int total = 0;
		
		for (Note tempNote : allEmployee) {
			if (tempNote.getId() == noteId) {
				if (noteDetails.getTitle() != null || noteDetails.getContent() != null) {
					total = noteRepository.updateNotes(noteDetails.getTitle(), noteDetails.getContent(), noteId);
				}
			}
		}
		
		hashResult.put("Message", "Update Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
	// Delete existed note
	@DeleteMapping("/delete-notes")
	public HashMap<String, Object> deleteNote(@RequestParam(name = "id") Long noteId) {
		HashMap<String, Object> hashResult = new HashMap<String, Object>();
		int total = noteRepository.deleteNotes(noteId);
		
		hashResult.put("Message", "Delete Success");
		hashResult.put("Total", total);
		
		return hashResult;
	}
	
}
