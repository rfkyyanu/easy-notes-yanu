package com.example.easynotes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.easynotes.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
	@Query(value = "SELECT * FROM Employee;", nativeQuery = true)
	List<Employee> getAllEmployee();
	
	@Modifying
	@Query(value = "INSERT INTO Employee (name, address, age, job_description, salary) VALUES (:name, :address, :age, :jobDesc, :salary)", nativeQuery = true)
	@Transactional
	int createEmployee(@Param("name") String name,
					   @Param("address") String address, 
					   @Param("age") Integer age, 
					   @Param("jobDesc") String jobDescription,
					   @Param("salary") Double salary);
	
	@Modifying
	@Query(value = "UPDATE Employee SET name = :name, address = :address, age = :age, job_description = :jobDesc, salary = :salary WHERE id = :id", nativeQuery = true)
	@Transactional
	int updateEmployee(@Param("name") String name,
					   @Param("address") String address, 
					   @Param("age") Integer age, 
					   @Param("jobDesc") String jobDescription,
					   @Param("salary") Double salary, 
					   @Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Employee WHERE id = :id", nativeQuery = true)
	@Transactional
	int deleteEmployee(@Param("id") Long id);
	
}
