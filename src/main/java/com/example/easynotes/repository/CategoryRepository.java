package com.example.easynotes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.easynotes.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

}
