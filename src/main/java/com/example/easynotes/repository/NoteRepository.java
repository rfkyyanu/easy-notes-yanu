package com.example.easynotes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.easynotes.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long>{
	
	@Query(value = "SELECT * FROM Notes;", nativeQuery = true)
	List<Note> getAllNotes();
	
	@Modifying
	@Query(value = "INSERT INTO Notes (title, content, created_at, updated_at) VALUES (:title, :content, NOW(), NOW())", nativeQuery = true)
	@Transactional
	int createNotes(@Param("title") String title,
					@Param("content") String content);
	
	@Modifying
	@Query(value = "UPDATE Notes SET title = :title, content = :content, updated_at = NOW() WHERE id = :id", nativeQuery = true)
	@Transactional
	int updateNotes(@Param("title") String title,
			   	    @Param("content") String content,
					@Param("id") Long id);
	
	@Modifying
	@Query(value = "DELETE FROM Notes WHERE id = :id", nativeQuery = true)
	@Transactional
	int deleteNotes(@Param("id") Long id);
	
}
